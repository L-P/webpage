---
title: "Monado - Running on Tegra"
layout: main
---

glslang is not available on Ubuntu 18.04. It can be compiled and installed with the usual procedure:

```
https://github.com/KhronosGroup/glslang.git
cd glslang
cmake . -G Ninja -DCMAKE_INSTALL_PREFIX=/usr -Bbuild
ninja -C build install
```

Otherwise the instructions from [Getting Started]({% link getting-started.md %}) work on Tegra for extended mode on X11.

# Direct Mode

The Vulkan driver for nvidia Tegra does not support vulkan based direct mode (VK_EXT_acquire_xlib_display, VK_EXT_direct_mode_display).

[Response from nvidia](https://forums.developer.nvidia.com/t/vk-ext-direct-mode-display-not-present-in-vulkan-1-2-on-jetpack-4-4/161593/2):

> VK_EXT_direct_mode_display is not required for doing direct mode through Vulkan on Jetson. VK_KHR_display and VK_KHR_display_swapchain are supported and those can be used for direct display mode.

See the `Vulkan on KMS/DRM` section in [Direct Mode]({% link direct-mode.md %}) for details of the implementation in Monado.

## Applications that require X11

Graphical applications do not properly connect to a normally running X server.

It is possible to run an offscreen X server that runs graphical applications.

Save a modified copy of the xorg.conf file as

/etc/X11/xorg.conf.offscreen
```
# Copyright (c) 2011-2013 NVIDIA CORPORATION.  All Rights Reserved.

#
# This is the minimal configuration necessary to use the Tegra driver.
# Please refer to the xorg.conf man page for more configuration
# options provided by the X server, including display-related options
# provided by RandR 1.2 and higher.

Section "ServerLayout"
    Identifier     "Layout0"
    Screen      0  "Screen0"
EndSection

Section "Monitor"
    Identifier     "Monitor0"
    VendorName     "Unknown"
    ModelName      "Unknown"
    HorizSync       28.0 - 33.0
    VertRefresh     43.0 - 72.0
    Option         "DPMS"
EndSection

Section "Screen"
    Identifier     "Screen0"
    Device         "Device0"
    Monitor        "Monitor0"
        SubSection     "Display"
                Virtual     1920 1080
        EndSubSection
    Option "UseDisplayDevice" "none"
EndSection

Section "Module"
    Disable     "dri"
    SubSection  "extmod"
        Option  "omit xfree86-dga"
    EndSubSection
EndSection

Section "Device"
    Identifier  "Tegra0"
    Driver      "nvidia"
# Allow X server to be started even if no display devices are connected.
    Option      "AllowEmptyInitialConfiguration" "true"
EndSection
```

Then run an X server with the command `Xorg -config ./xorg.conf.offscreen :10`. It should drop to a text vt console.

Now it is possible to use both `XRT_COMPOSITOR_FORCE_VK_DISPLAY=0` to let the Monado compositor use the display, and to start an OpenGL application that requires an X server with `DISPLAY=:10`, for example `DISPLAY=:10 hello_xr -G OpenGL`.
