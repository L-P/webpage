---
title: "Monado - Multi GPU"
layout: main
---

* TOC
{:toc}

# Intel integrated GPU + NVidia dedicated GPU.

The following configurations have been tested on Ubuntu 20.04 with UHD Graphics 610 using Mesa 20.0.8 and a GeForce GTX 1050 Ti using nvidia 440.64.

`sudo prime-select nvidia` and `sudo prime-select intel` can switch between using the nvidia or the intel GPU as primary GPU (the GPU the X Server primarily runs on). Reboot required after switch.

## 1. prime-select nvidia

### 1.1 monitor connected to intel gpu, HMD connected to nvidia gpu

Note: nvidia may choose an nvidia-only setup when there are monitors connected to nvidia. When connecting monitors to the intel GPU, connect ONLY a HMD to the nvidia gpu.

* monado-service on nvidia gpu: Direct mode works
    * Vulkan client on nvidia GPU: works
    * Vulkan client on intel GPU: `vkAllocateMemory: VK_ERROR_INVALID_EXTERNAL_HANDLE`
    * OpenGL client on nvidia GPU: works
    * OpenGL client on intel GPU: n/a (unknown how to start an application like this)

* monado-service on intel gpu: no direct mode, compositor window renders only black/locks up?
    * Vulkan client: locks up, likely blocked on blocked compositor
    * `XRT_COMPOSITOR_FORCE_NVIDIA=1` will not work, intel driver will not detect HMD on nvidia GPU

### 1.2 monitor connected to nvidia GPU, HMD connected to intel GPU

Same as 1.1.

### 1.3 monitor connected to intel GPU, HMD connected to intel GPU

* monado-service on intel GPU: vkAcquireXlibDisplayEXT: VK_ERROR_INITIALIZATION_FAILED
    * probable explanation: intel driver can see HMD display output, but running with nvidia as primary GPU, direct mode is not properly supported in this reverse prime configuration
* monado-service on nvidia GPU: X Error
    * probable explanation: nvidia driver sees HMD display connected to intel GPU, but fails to use it with direct mode
    
```
X Error of failed request:  BadMatch (invalid parameter attributes)
  Major opcode of failed request:  156 (NV-GLX)
  Minor opcode of failed request:  31 ()
  Serial number of failed request:  31
  Current serial number in output stream:  31
```

### Note for 1.1 and 1.2

When running in `prime-select nvidia` mode, other Vulkan applications like vkmark also lock up with a black window when attempting to run on the intel GPU. (see https://github.com/vkmark/vkmark/pull/28 for GPU selection in vkmark)

## 2. prime-select intel

Completely disables the nvidia GPU on my desktop PC. Please contribute to this article if you have an Optimus Laptop that behaves differently.

### 2.1 monitor connected to intel GPU, HMD connected to intel GPU

* monado-service on intel gpu: Direct mode works
    * Vulkan client on intel GPU: works
    * OpenGL client on nvidia GPU: works
